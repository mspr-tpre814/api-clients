FROM maven:3.9.6 AS builder
ENV DB_HOST=
ENV DB_PORT=
ENV DB_DATABASE=
ENV DB_USER=
ENV DB_PASSWORD=
ENV MQ_HOST=
ENV MQ_PORT=
ENV MQ_USER=
ENV MQ_PASSWORD=
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

FROM openjdk:21-jdk
WORKDIR /app
COPY --from=builder /app/target/customers-api.jar .

CMD ["java", "-jar", "customers-api.jar"]