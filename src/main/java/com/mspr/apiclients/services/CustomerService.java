package com.mspr.apiclients.services;

import com.mspr.apiclients.entities.Customer;

import java.util.List;

/**
 * CustomerService
 * CustomerService is the interface used to define the methods for the Customer entity
 */

public interface CustomerService {
        List<Customer> getCustomers();
        Customer getCustomer(String id);
        Customer createCustomer(Customer customer);
        Customer updateCustomer(String uid, Customer customer);
        void deleteCustomer(String id);
        boolean checkCustomerPresence(String id);
}
