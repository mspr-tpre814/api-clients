package com.mspr.apiclients.services.impl;

import com.mspr.apiclients.entities.Customer;
import com.mspr.apiclients.repositories.CustomerRepository;
import com.mspr.apiclients.services.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

/**
 * Implementation of the CustomerService
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    /**
     * Check if a customer exists
     * @param id the id of the customer
     * @return true if the customer exists, false otherwise
     */
    @Override
    @RabbitListener(queues = "customerPresenceQueue")
    public boolean checkCustomerPresence(String id) {
        boolean result = customerRepository.existsById(id);
        log.info("Customer presence check for id: {} result: {}", id, result);
        return result;
    }

    /**
     * Get all customers
     * @return the list of customers
     */
    @Override
    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    /**
     * Get a customer by its id
     * @param id the id of the customer
     * @return the customer
     */
    @Override
    public Customer getCustomer(String id) {
        return customerRepository.findById(id).orElseThrow();
    }

    /**
     * Create a customer
     * @param customer the customer to create
     * @return the created customer
     */
    @Override
    public Customer createCustomer(Customer customer) {
        customer.setCreatedAt(Instant.now());
        return customerRepository.save(customer);
    }

    /**
     * Update a customer
     * @param id the id of the customer
     * @param customer the customer to update
     * @return the updated customer
     */
    @Override
    public Customer updateCustomer(String id, Customer customer) {
        return customerRepository.save(customer);
    }

    /**
     * Delete a customer
     * @param id the id of the customer
     */
    @Override
    public void deleteCustomer(String id) {
        customerRepository.deleteById(id);
    }

}
