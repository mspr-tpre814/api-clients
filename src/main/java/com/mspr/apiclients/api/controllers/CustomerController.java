package com.mspr.apiclients.api.controllers;

import com.mspr.apiclients.api.dto.CustomerRequestDTO;
import com.mspr.apiclients.api.dto.CustomerResponseDTO;
import com.mspr.apiclients.api.mapper.Mapper;
import com.mspr.apiclients.services.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * CustomerController
 * This class is the controller for the Customer entity.
 * It handles the CRUD operations for the Customer entity.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/customers", produces = APPLICATION_JSON_VALUE)
public class CustomerController {
    private final CustomerService customerService;
    private final Mapper mapper;

    /**
     * This method returns all the customers.
     *
     * @return ResponseEntity<List<CustomerResponseDTO>> This returns the list of customers.
     */
    @GetMapping("/")
    public ResponseEntity<List<CustomerResponseDTO>> getAllCustomers() {
        List<CustomerResponseDTO> clients = customerService.getCustomers()
                .stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    /**
     * This method returns a customer by its id.
     *
     * @param id The id of the customer.
     * @return ResponseEntity<CustomerResponseDTO> This returns the customer.
     */
    @GetMapping("/{id}")
    public ResponseEntity<CustomerResponseDTO> getCustomer(@PathVariable String id) {
        return new ResponseEntity<>(mapper.toDTO(customerService.getCustomer(id)), HttpStatus.OK);
    }

    /**
     * This method creates a customer.
     *
     * @param customerRequestDTO The customer to create.
     * @return ResponseEntity<CustomerResponseDTO> This returns the created customer.
     */
    @PostMapping("/")
    public ResponseEntity<CustomerResponseDTO> createCustomer(@RequestBody CustomerRequestDTO customerRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(customerService.createCustomer(mapper.toEntity(customerRequestDTO))), HttpStatus.CREATED);
    }

    /**
     * This method updates a customer.
     *
     * @param id The id of the customer.
     * @param customerRequestDTO The customer to update.
     * @return ResponseEntity<CustomerResponseDTO> This returns the updated customer.
     */
    @PutMapping("/{id}")
    public ResponseEntity<CustomerResponseDTO> updateCustomer(@PathVariable String id, @RequestBody CustomerRequestDTO customerRequestDTO) {
        return new ResponseEntity<>(mapper.toDTO(customerService.updateCustomer(id, mapper.toEntity(customerRequestDTO))), HttpStatus.OK);
    }

    /**
     * This method deletes a customer.
     *
     * @param id The id of the customer.
     * @return ResponseEntity<?> This returns an empty response.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable String id) {
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
