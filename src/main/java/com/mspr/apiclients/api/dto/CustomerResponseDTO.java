package com.mspr.apiclients.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

/**
 * CustomerResponseDTO
 * CustomerResponseDTO is the DTO used to return a customer
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResponseDTO {
    private String id;
    private Instant createdAt;
    private String name;
    private String username;
    private String firstname;
    private String lastname;
    private AddressDTO address;
    private ProfileDTO profile;
    private CompanyDTO company;
}
