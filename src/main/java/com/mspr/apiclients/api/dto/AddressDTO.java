package com.mspr.apiclients.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AddressDTO
 * AddressDTO is the DTO used to return an address
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {
    private String postalCode;
    private String city;
}
