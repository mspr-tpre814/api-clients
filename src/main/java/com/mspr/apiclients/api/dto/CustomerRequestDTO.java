package com.mspr.apiclients.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AddressDTO
 * AddressDTO is the DTO used to return an address
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequestDTO {
    private String name;
    private String username;
    private String firstName;
    private String lastName;
    private AddressDTO address;
    private ProfileDTO profile;
    private CompanyDTO company;
}
