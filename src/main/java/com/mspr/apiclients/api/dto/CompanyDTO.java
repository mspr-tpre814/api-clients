package com.mspr.apiclients.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CompanyDTO
 * CompanyDTO is the DTO used to return a company
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDTO {
    private String companyName;
}
