package com.mspr.apiclients.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AddressDTO
 * AddressDTO is the DTO used to return an address
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDTO {
    private String firstName;
    private String lastName;
}
