package com.mspr.apiclients.api.mapper;

import com.mspr.apiclients.api.dto.*;
import com.mspr.apiclients.entities.Address;
import com.mspr.apiclients.entities.Company;
import com.mspr.apiclients.entities.Customer;
import com.mspr.apiclients.entities.Profile;
import org.springframework.stereotype.Component;

/**
 * Mapper class to convert entities to DTOs and vice versa
 */

@Component
public class Mapper {
    /**
     * Convert a Customer entity to a CustomerResponseDTO
     * @param customer the Customer entity to convert
     * @return the converted CustomerResponseDTO
     */
    public CustomerResponseDTO toDTO(Customer customer) {
        return new CustomerResponseDTO(
                customer.getId(),
                customer.getCreatedAt(),
                customer.getName(),
                customer.getUsername(),
                customer.getFirstname(),
                customer.getLastname(),
                toDTO(customer.getAddress()),
                toDTO(customer.getProfile()),
                toDTO(customer.getCompany())
        );
    }

    /**
     * Convert a CustomerRequestDTO to a Customer entity
     * @param customerRequestDTO the CustomerRequestDTO to convert
     * @return the converted Customer entity
     */
    public Customer toEntity(CustomerRequestDTO customerRequestDTO) {
        return new Customer(
                customerRequestDTO.getName(),
                customerRequestDTO.getUsername(),
                customerRequestDTO.getFirstName(),
                customerRequestDTO.getLastName(),
                toEntity(customerRequestDTO.getAddress()),
                toEntity(customerRequestDTO.getProfile()),
                toEntity(customerRequestDTO.getCompany())
        );
    }

    /**
     * Convert an Address entity to an AddressDTO
     * @param address the Address entity to convert
     * @return the converted AddressDTO
     */
    public Address toEntity(AddressDTO addressDTO) {
        return new Address(
                addressDTO.getPostalCode(),
                addressDTO.getCity()
        );
    }

    /**
     * Convert an AddressDTO to an Address entity
     * @param addressDTO the AddressDTO to convert
     * @return the converted Address entity
     */
    public AddressDTO toDTO(Address address) {
        return new AddressDTO(
                address.getPostalCode(),
                address.getCity()
        );
    }

    /**
     * Convert a Company entity to a CompanyDTO
     * @param company the Company entity to convert
     * @return the converted CompanyDTO
     */
    public Company toEntity(CompanyDTO companyDTO) {
        return new Company(
                companyDTO.getCompanyName()
        );
    }

    /**
     * Convert a CompanyDTO to a Company entity
     * @param company the CompanyDTO to convert
     * @return the converted Company entity
     */
    public CompanyDTO toDTO(Company company) {
        return new CompanyDTO(
                company.getCompanyName()
        );
    }

    /**
     * Convert a Profile entity to a ProfileDTO
     * @param profile the Profile entity to convert
     * @return the converted ProfileDTO
     */
    public Profile toEntity(ProfileDTO profileDTO) {
        return new Profile(
                profileDTO.getFirstName(),
                profileDTO.getLastName()
        );
    }

    /**
     * Convert a ProfileDTO to a Profile entity
     * @param profile the ProfileDTO to convert
     * @return the converted Profile entity
     */
    public ProfileDTO toDTO(Profile profile) {
        return new ProfileDTO(
                profile.getFirstName(),
                profile.getLastName()
        );
    }
}
