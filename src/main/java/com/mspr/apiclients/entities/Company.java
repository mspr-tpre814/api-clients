package com.mspr.apiclients.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Company
 * Company is the entity used to store a company
 */

@Entity
@Table(name = "company")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Company {
    @Id
    @Column(name = "company_name")
    private String companyName;

    @OneToOne(mappedBy = "company")
    @JsonIgnore
    private Customer customer;

    public Company(String companyName) {
        this.companyName = companyName;
    }
}
