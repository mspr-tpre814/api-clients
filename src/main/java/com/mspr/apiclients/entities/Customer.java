package com.mspr.apiclients.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

/**
 * Customer
 * Customer is the entity used to store a customer
 */

@Entity
@Table(name = "customer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "created_at")
    private Instant createdAt;
    @Column(name = "name")
    private String name;
    @Column(name = "username")
    private String username;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "profile_id", referencedColumnName = "id")
    private Profile profile;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_company_name", referencedColumnName = "company_name")
    private Company company;

    public Customer(String name, String username, String firstname, String lastname, Address entity, Profile entity1, Company entity2) {
        this.name = name;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = entity;
        this.profile = entity1;
        this.company = entity2;
    }

}
