package com.mspr.apiclients.repositories;

import com.mspr.apiclients.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for the Customer entity
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {
}
