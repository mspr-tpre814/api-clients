package com.mspr.apiclients.services.impl;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.mspr.apiclients.entities.Customer;
import com.mspr.apiclients.repositories.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    private Customer customer;

    @Before
    public void setUp() {
        customer = new Customer("1", Instant.now(), "Customer1", "username1", "firstname1", "lastname1", null, null, null);
    }

    @Test
    public void getCustomersTest() {
        when(customerRepository.findAll()).thenReturn(Collections.singletonList(customer));
        List<Customer> customers = customerService.getCustomers();
        assertFalse(customers.isEmpty());
        assertEquals(customers.get(0).getName(), "Customer1");
    }

    @Test
    public void getCustomerTest() {
        when(customerRepository.findById(anyString())).thenReturn(Optional.of(customer));
        Customer foundCustomer = customerService.getCustomer("1");
        assertNotNull(foundCustomer);
        assertEquals(foundCustomer.getName(), "Customer1");
    }

    @Test
    public void createCustomerTest() {
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);
        Customer createdCustomer = customerService.createCustomer(customer);
        assertNotNull(createdCustomer);
        assertEquals(createdCustomer.getName(), "Customer1");
    }

    @Test
    public void updateCustomerTest() {
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);
        Customer updatedCustomer = customerService.updateCustomer("1", customer);
        assertNotNull(updatedCustomer);
        assertEquals(updatedCustomer.getName(), "Customer1");
    }

    @Test
    public void deleteCustomerTest() {
        doNothing().when(customerRepository).deleteById(anyString());
        customerService.deleteCustomer("1");
        verify(customerRepository, times(1)).deleteById("1");
    }

    @Test
    public void checkCustomerAvailabilityTest() {
        when(customerRepository.existsById(anyString())).thenReturn(true);
        boolean isAvailable = customerService.checkCustomerPresence("1");
        assertTrue(isAvailable);
    }

}
