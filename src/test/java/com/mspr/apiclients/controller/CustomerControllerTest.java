package com.mspr.apiclients.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.mspr.apiclients.api.controllers.CustomerController;
import com.mspr.apiclients.api.dto.CustomerRequestDTO;
import com.mspr.apiclients.api.dto.CustomerResponseDTO;
import com.mspr.apiclients.api.mapper.Mapper;
import com.mspr.apiclients.entities.Customer;
import com.mspr.apiclients.services.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private Mapper mapper;

    private Customer customer;
    private CustomerResponseDTO customerResponseDTO;

    @Before
    public void setUp() {
        customer = new Customer("1", Instant.now(), "Customer1", "username1", "firstname1", "lastname1", null, null, null);
        customerResponseDTO = new CustomerResponseDTO("1", Instant.now(), "Customer1", "username1", "firstname1", "lastname1", null, null, null);
    }

    @Test
    public void getAllCustomersTest() throws Exception {
        List<Customer> customerList = Collections.singletonList(customer);
        List<CustomerResponseDTO> customerResponseDTOList = Collections.singletonList(customerResponseDTO);

        when(customerService.getCustomers()).thenReturn(customerList);
        when(mapper.toDTO(customer)).thenReturn(customerResponseDTO);

        mockMvc.perform(get("/customers/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(customer.getName())))
                .andExpect(jsonPath("$[0].username", is(customer.getUsername())))
                .andExpect(jsonPath("$[0].firstname", is(customer.getFirstname())))
                .andExpect(jsonPath("$[0].lastname", is(customer.getLastname())));
    }

    @Test
    public void getCustomerTest() throws Exception {
        when(customerService.getCustomer(anyString())).thenReturn(customer);
        when(mapper.toDTO(customer)).thenReturn(customerResponseDTO);

        mockMvc.perform(get("/customers/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name", is(customer.getName())))
                .andExpect(jsonPath("$.username", is(customer.getUsername())))
                .andExpect(jsonPath("$.firstname", is(customer.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(customer.getLastname())));
    }

    @Test
    public void createCustomerTest() throws Exception {
        when(customerService.createCustomer(any(Customer.class))).thenReturn(customer);
        when(mapper.toDTO(any(Customer.class))).thenReturn(customerResponseDTO);
        when(mapper.toEntity(any(CustomerRequestDTO.class))).thenReturn(customer);

        mockMvc.perform(post("/customers/")
                        .contentType("application/json")
                        .content("{\"name\":\"Customer1\",\"username\":\"username1\",\"firstname\":\"firstname1\",\"lastname\":\"lastname1\"}"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name", is(customer.getName())))
                .andExpect(jsonPath("$.username", is(customer.getUsername())))
                .andExpect(jsonPath("$.firstname", is(customer.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(customer.getLastname())));
    }

    @Test
    public void updateCustomerTest() throws Exception {
        when(customerService.updateCustomer(anyString(), any(Customer.class))).thenReturn(customer);
        when(mapper.toDTO(any(Customer.class))).thenReturn(customerResponseDTO);
        when(mapper.toEntity(any(CustomerRequestDTO.class))).thenReturn(customer);

        mockMvc.perform(put("/customers/1")
                        .contentType("application/json")
                        .content("{\"name\":\"Customer1\",\"username\":\"username1\",\"firstname\":\"firstname1\",\"lastname\":\"lastname1\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name", is(customer.getName())))
                .andExpect(jsonPath("$.username", is(customer.getUsername())))
                .andExpect(jsonPath("$.firstname", is(customer.getFirstname())))
                .andExpect(jsonPath("$.lastname", is(customer.getLastname())));
    }

    @Test
    public void deleteCustomerTest() throws Exception {
        mockMvc.perform(delete("/customers/1"))
                .andExpect(status().isOk());
    }
}

