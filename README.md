# Customer Service Documentation

## Table des matières
- [Schéma d'Architecture](#Schéma-dArchitecture)
- [Sécurité de l'API](#Sécurité-de-lAPI)
- [Langage de programmation](#Langage-de-programmation)
- [Base de données](#Base-de-données)
- [Gestion du Code Source](#Gestion-du-Code-Source)

## Schéma d'Architecture

Le microservice Customer est conçu selon une architecture basée sur des microservices. Il est indépendant et communique avec les autres services via des APIs REST, avec une synchronisation des données assurée par RabbitMQ.

## Sécurité de l'API

L'API REST du microservice Customer est sécurisée par un système de clé API. Chaque requête doit être authentifiée avec une clé API valide, qui est générée pour chaque client et qui est utilisée pour identifier le client et autoriser l'accès à l'API.

## Langage de programmation

Le microservice Customer est développé en Java, avec le framework Spring Boot pour la gestion des APIs REST et la synchronisation des données avec RabbitMQ.

Spring Data JPA: Fournit une abstraction sur les bases de données relationnelles et permet une interaction facile avec PostgreSQL.

## Base de données

Le microservice Customer utilise une base de données PostGre pour stocker les données des clients. La base de données est hébergée sur un serveur PostGre et est accessible via une connexion JDBC.

![screenshot](bdd_customer.png)

## Gestion du Code Source

Le microservice Customer dispose de son propre repository Git pour un développement indépendant.

GitFlow: Utilisation de la méthodologie GitFlow pour gérer les branches. Les branches main et develop sont les principales, avec des branches de fonctionnalités, de release, et de hotfix.
